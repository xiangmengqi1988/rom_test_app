package com.xmq.rom_test;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class MainActivity extends Activity {
	private static final String LOG_TAG = "HelloCamera";
	private static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 100;
	private static final int CAPTURE_VIDEO_ACTIVITY_REQUEST_CODE = 200;

	private Button TakePicBtn = null;
	private ImageView imageView = null;
	private Uri fileUri;

	public static final int MEDIA_TYPE_IMAGE = 1;
	// 视频的扩展变量
	public static final int MEDIA_TYPE_VIDEO = 2;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.d(LOG_TAG, "onCreate");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TakePicBtn = (Button) findViewById(R.id.TakePicture);
		TakePicBtn.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				Log.d(LOG_TAG, "Take picture Button Click");
				// 利用系统自带的相机应用拍照
				Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
				// 获得照片的存放路径
				fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
				// 设置照片名字
				// 此处这句intent的值设置关系到后面的onActivityResult中会进入那个分支
				// 关系到data是否为null,如果此处制则后来的data为Nullll
				intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

				startActivityForResult(intent,
						CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
			}
		});
		imageView = (ImageView) findViewById(R.id.ImageViewer);

	}

	// creat a file Uri for saving an image or video
	private static Uri getOutputMediaFileUri(int type) {
		return Uri.fromFile(getOutputMediaFile(type));
	}

	// creat a file Uri or saving an image or video
	private static File getOutputMediaFile(int type) {
		// To be safe, you should check that the SDCard is mounted
		// using Environment.getExternalStorageState() before doing this.

		File mediaStorageDir = null;
		try {
			// This location works best if you want the created images to be
			// shared
			// between applications and persist after your app has been
			// uninstalled.
			mediaStorageDir = new File(
					Environment
							.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
					"MyCameraApp");

			Log.d(LOG_TAG, "Successfully created mediaStorageDir: "
					+ mediaStorageDir);

		} catch (Exception e) {
			e.printStackTrace();
			Log.d(LOG_TAG, "Error in Creating mediaStorageDir: "
					+ mediaStorageDir);
		}

		// Create the storage directory if it does not exist
		if (!mediaStorageDir.exists()) {
			if (!mediaStorageDir.mkdirs()) {
				// 在SD卡上创建文件夹需要权限：
				// <uses-permission
				// android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
				Log.d(LOG_TAG,
						"failed to create directory, check if you have the WRITE_EXTERNAL_STORAGE permission");
				return null;
			}
		}

		// Create a media file name
		SimpleDateFormat timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss");
		Date data = new Date();
		String TimeStampStrs = timeStamp.format(data);
		File mediaFile;
		if (type == MEDIA_TYPE_IMAGE) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "IMG_" + TimeStampStrs + ".jpg");
		} else if (type == MEDIA_TYPE_VIDEO) {
			mediaFile = new File(mediaStorageDir.getPath() + File.separator
					+ "VID_" + TimeStampStrs + ".mp4");
		} else {
			return null;
		}

		return mediaFile;
	}
}
